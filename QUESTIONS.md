# Questions

For this assignment you also have to answer a couple of questions.
There is no correct answer and none is mandatory, if you don't know just skip it.

 - **What do you think of the initial project structure ?**

    I think it is good because the logic layer and database layer is separated well and each class have their own responsibility.

 - **What you will improve from your solution ?**
    - Create router class abstraction. Right now my api routing is using gorilla/mux library and put inside adapter class. I think it should be better to create abstraction class rather than just using it. 


 - **For you, what are the boundaries of a service inside a micro-service architecture ?**
 
    I think a service should follow this criteria:

    - Each service has a single responsibility.
    - Services are not tightly coupled, and can evolve independently.
    - Each service is small enough that it can be built by a small team working independently
    



 - **For you, what are the most relevant usage for SQL, NoSQL, key-value and document store ?**

    SQL is a good choice for any business that will benefit from its pre-defined structure and set schemas. For example, applications that require multi-row transactions - like accounting systems or systems that monitor inventory - or that run on legacy systems will thrive with the MySQL structure.
    
    NoSQL document store, is a good choice for businesses that have rapid growth or databases with no clear schema definitions. Especially if you often denormalizing data schemas, or if your schema continues to change. For example mobile apps, real-time analytics, content management systems.
    
    NoSQL Key-value dbs are typically simpler, both in terms of API and content. You would use one for when you need small snippets of data handily available, like a hit counter or session data or votes.
