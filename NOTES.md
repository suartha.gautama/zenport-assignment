# Notes

*Database setup, structure change, tests setup, etc.*
# Preparation
- clone this repository to `$GOPATH/src/gitlab.com/zenport.io/go-assignment`
- run `dep ensure`.
# Database setup

- execute dockerfile:
```shell
docker build . -t go-assignment
```
- run Docker in background:
```shell
docker run -d -p 32779:5432 go-assignment
```

# Test setup

- Run test using this command:
```shell
docker run -d -p 32779:5432 go-assignment
```

# Running API
- You can also run the API using:
```shell
go run main.go
```


# Stop Docker
- Don't forget to stop docker after running test
```shell
docker stop [CONTAINER ID]
```