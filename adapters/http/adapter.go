package http

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/zenport.io/go-assignment/domain"
	"gitlab.com/zenport.io/go-assignment/engine"
)

type HTTPAdapter struct {
	Router *mux.Router
	Engine engine.Engine
	server *http.Server
}

func (adapter *HTTPAdapter) InitializeRoutes() {
	adapter.Router.HandleFunc("/knight", adapter.getlistKnight).Methods("GET")
	adapter.Router.HandleFunc("/knight/{id}", adapter.getKnight).Methods("GET")
	adapter.Router.HandleFunc("/knight", adapter.saveKnight).Methods("POST")
}

func (adapter *HTTPAdapter) InitializeServer() {
	adapter.server = &http.Server{Addr: ":8080", Handler: adapter.Router}
}

func (adapter *HTTPAdapter) Start() {
	if err := adapter.server.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}

func (adapter *HTTPAdapter) Stop() {
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	if err := adapter.server.Shutdown(ctx); err != nil {
		log.Fatal(err)
	}
}

func (adapter *HTTPAdapter) getlistKnight(w http.ResponseWriter, r *http.Request) {
	knights := adapter.Engine.ListKnights()
	respondWithJSON(w, http.StatusOK, knights)
}

func (adapter *HTTPAdapter) getKnight(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	knight, err := adapter.Engine.GetKnight(id)
	if err != nil {
		respondWithError(w, http.StatusNotFound, err)
		return
	}
	respondWithJSON(w, http.StatusOK, knight)
}

func (adapter *HTTPAdapter) saveKnight(w http.ResponseWriter, r *http.Request) {
	var k domain.Knight

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&k); err != nil {
		respondWithError(w, http.StatusBadRequest, err)
		return
	}

	defer r.Body.Close()
	if k.Strength == 0 {
		respondWithError(w, http.StatusBadRequest, errors.New(fmt.Sprintf("Expected strength field")))
		return
	}
	if k.WeaponPower == 0 {
		respondWithError(w, http.StatusBadRequest, errors.New(fmt.Sprintf("Expected weapon_power field")))
		return
	}
	_, err := adapter.Engine.CreateKnight(k)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, errors.New(fmt.Sprintf("Database Error, Cannot create new data")))
		return
	}
	respondWithJSON(w, http.StatusCreated, k)
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func respondWithError(w http.ResponseWriter, code int, message error) {
	respondWithJSON(w, code, map[string]string{"code": strconv.Itoa(code), "message": message.Error()})
}

func NewHTTPAdapter(e engine.Engine) *HTTPAdapter {

	adapter := HTTPAdapter{Router: mux.NewRouter(), Engine: e}
	adapter.InitializeRoutes()
	adapter.InitializeServer()
	return &adapter
}
