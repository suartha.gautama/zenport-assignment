create role user1 login password 'user1';
create database assignment;
grant all privileges on database assignment to user1;
\c assignment;
CREATE TABLE IF NOT EXISTS knight
						(
						id SERIAL,
						name varchar(60) NOT NULL,
						strength integer NOT NULL,
						weaponpower integer NOT NULL,
						CONSTRAINT knight_pkey PRIMARY KEY (id)
						);
GRANT ALL PRIVILEGES ON TABLE knight TO user1;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public to user1;
create database assignment_test;
grant all privileges on database assignment_test to user1;

