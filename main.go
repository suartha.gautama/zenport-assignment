package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/zenport.io/go-assignment/adapters/http"
	"gitlab.com/zenport.io/go-assignment/engine"
	"gitlab.com/zenport.io/go-assignment/providers/database"
)

const (
	host     = "localhost"
	port     = 32779
	dbname   = "assignment"
	username = "user1"
	password = "user1"
)

func main() {

	connectionString := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable", host, port, username, password, dbname)
	DB, err := sqlx.Connect("postgres", connectionString)
	if err != nil {
		log.Fatalln(err)
	}
	provider := database.NewProvider(DB)
	e := engine.NewEngine(provider)

	adapter := http.NewHTTPAdapter(e)
	adapter.Start()
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGINT, syscall.SIGTERM)
	defer close(stop)

	<-stop

	adapter.Stop()
	provider.Close()
}
