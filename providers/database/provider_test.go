package database

import (
	"fmt"
	"os"
	"testing"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/zenport.io/go-assignment/domain"
)

const (
	host     = "localhost"
	port     = 32779
	dbname   = "assignment_test"
	username = "user1"
	password = "user1"
)

var (
	repository *knightRepository
)

func TestMain(m *testing.M) {

	connectionString := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable", host, port, username, password, dbname)
	DB, err := sqlx.Open("postgres", connectionString)
	if err != nil {
		panic(err)
	}
	defer DB.Close()

	createTable(DB)

	repository = &knightRepository{db: DB}
	code := m.Run()
	dropTable(DB)
	DB.Close()
	os.Exit(code)

}

func TestInsertKnightBaku(t *testing.T) {

	k := domain.Knight{Name: "Baku", Strength: 5, WeaponPower: 6}
	rowAffected, err := repository.Save(k)
	if err != nil {
		t.Fatal(err)
	}
	if rowAffected != 1 {
		t.Fatal("Error inserting to database, affected row should be 1 instead of", rowAffected)
	}
}

func TestFindBaku(t *testing.T) {
	toFindID := "1"
	knight := repository.Find(toFindID)
	if knight == nil {
		t.Fatal("Cannot find knight with id ", toFindID, " is not found")
	}
}

func TestFindNotFound(t *testing.T) {
	toFindID := "23121"
	knight := repository.Find(toFindID)
	if knight != nil {
		t.Fatal("Expected zero result but found data ")
	}
}

func TestFindAll(t *testing.T) {
	knights := repository.FindAll()
	if len(*knights) != 1 {
		t.Fatal("Response error: Expected 1 knights")
	}

}

func dropTable(DB *sqlx.DB) {
	dropTableQuery := `DROP TABLE knight`
	_, err := DB.Exec(dropTableQuery)
	if err != nil {
		panic(err)
	}
}

func createTable(DB *sqlx.DB) {

	tableCreationQuery := `CREATE TABLE IF NOT EXISTS knight
						(
						id SERIAL,
						name varchar(60) NOT NULL,
						strength integer NOT NULL,
						weaponpower integer NOT NULL,
						CONSTRAINT knight_pkey PRIMARY KEY (id)
						)`
	_, err := DB.Exec(tableCreationQuery)
	if err != nil {
		panic(err)
	}

}
