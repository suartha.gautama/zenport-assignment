package database

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/zenport.io/go-assignment/domain"
)

type knightRepository struct {
	db *sqlx.DB
}

func (repository *knightRepository) Find(ID string) *domain.Knight {
	result := domain.Knight{}
	err := repository.db.Get(&result, "SELECT id,name,strength,weaponpower from knight where id = $1", ID)

	if err != nil {
		return nil
	}
	return &result
}
func (repository *knightRepository) FindAll() *[]domain.Knight {
	knights := []domain.Knight{}
	repository.db.Select(&knights, "select * from knight")
	return &knights
}
func (repository *knightRepository) Save(knight domain.Knight) (int64, error) {

	knightSaveStatement := `INSERT INTO knight (name, strength, weaponpower) VALUES ($1, $2, $3)`
	result := repository.db.MustExec(knightSaveStatement, knight.Name, knight.Strength, knight.WeaponPower)
	return result.RowsAffected()
}
