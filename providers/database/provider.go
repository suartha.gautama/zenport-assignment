package database

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/zenport.io/go-assignment/engine"
)

type Provider struct {
	db *sqlx.DB
}

func (provider *Provider) GetKnightRepository() engine.KnightRepository {
	return &knightRepository{db: provider.db}
}

func (provider *Provider) Close() {
	provider.db.Close()
}

func NewProvider(db *sqlx.DB) *Provider {
	return &Provider{db: db}
}
