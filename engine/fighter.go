package engine

import (
	"errors"
	"fmt"

	"gitlab.com/zenport.io/go-assignment/domain"
)

func (engine *arenaEngine) GetKnight(ID string) (*domain.Knight, error) {
	knight := engine.knightRepository.Find(ID)
	if knight == nil {
		return nil, errors.New(fmt.Sprintf("Knight #%s not found.", ID))
	}
	return knight, nil
}

func (engine *arenaEngine) ListKnights() *[]domain.Knight {
	knights := engine.knightRepository.FindAll()
	return knights
}

func (engine *arenaEngine) Fight(fighter1ID string, fighter2ID string) domain.Fighter {
	return nil
}

func (engine *arenaEngine) CreateKnight(knight domain.Knight) (int64, error) {
	return engine.knightRepository.Save(knight)
}
