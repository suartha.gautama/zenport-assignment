package domain

type Fighter interface {
	GetID() string
	GetPower() float64
}

type Knight struct {
	ID          string `json:"id" db:"id"`
	Name        string `json:"name,omitempty" db:"name"`
	Strength    int    `json:"strength,omitempty" db:"strength"`
	WeaponPower int    `json:"weapon_power,omitempty" db:"weaponpower"`
}

func (k Knight) GetPower() int {
	return k.Strength + k.WeaponPower
}

func (k Knight) GetID() string {
	return k.ID
}
